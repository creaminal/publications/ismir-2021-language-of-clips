# ismir-2021-language-of-clips

This is the companion webpage of the paper "The language of music-video clips: a multimodal analysis" (Laure Prétet, Gaël Richard, Geoffroy Peeters).
It features the manual annotations of the Harmonix set official music videos into video genres.
The following genres are considered:  

- **Performance videos (P):**  The artist or band are presented performing the song. 74 videos; example: Iron Maiden
- **Concept/Abstract videos (C):** The video illustrates the music metaphorically via a series of abstract shots related to semantics or atmosphere of the song. 227 videos; example: Lady Gaga
- **Narrative videos (N):** The music video has a strong narrative content, with *identifiable characters* and an explicit *chronology*. 160 videos; example: Taylor Swift
- **Dance videos (D):** Artists present a *rehearsed dance choreography* in sync with the music. 62 videos: examples: Sean Paul
- **Other (O):** Other types of music videos, including lyrics videos, animated music videos, etc. 25 videos; example: Hey, Soul Sister (Train)
